========
Overview
========

What is Android D-Link Default KeyGenerator:


D-Link KeyGenerator is a simple, opensource application written in Java For android devices, 
witch main purpose is to provide default passwords for D-link routers. 


=============
How to use it
=============

The User Interface is pretty simple,just fill in the routers Mac-Address (eg. 00:22:B0:F0:29:AA) and it will instantly generate 
a key to access the network. Or Select A network and the rest will be done automatically.

=============
Details
=============

Android D-Link Default KeyGenerator

Author/Developer : Nuno Khan

Version 1.0

2012

=====================================
Technical Support
	nunok7@gmail.com
=====================================
