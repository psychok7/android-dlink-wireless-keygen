package com.psychok7.android.dlink_wireless_keygen;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class KeygenActivity extends Activity {

	final Context context = this;
	private Button btnSubmit, btn;
	private EditText text;
	private WifiManager wifi;
	private Spinner spinner;
	private List<ScanResult> scan;
	private Dialog dialog;
	private EditText edittext;
	private ImageView image;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		text = (EditText) findViewById(R.id.editText1);
		wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);

	}

	// get the selected dropdown list value
	public void addListenerOnButton() {
		spinner = (Spinner) findViewById(R.id.spinner);
		btnSubmit = (Button) findViewById(R.id.buttonShowNetworks);
		btnSubmit.setText("Choose Network and Generate Key");
		btnSubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				for (int i = 0; i < scan.size(); i++) {
					if (spinner.getSelectedItemPosition() == i) {
						initDialog();
						String key = new GenerateKey().generate(scan.get(i).BSSID
								.toString());

						if (key != null) {
							edittext.setText(key);
							dialog.show();
						}

						break;
					}
				}
				btn = (Button) findViewById(R.id.buttonShowNetworks);
				btn.setVisibility(View.VISIBLE);

			}
		});
	}

	// add items into spinner dynamically
	public void addItemsOnSpinner() {

		spinner = (Spinner) findViewById(R.id.spinner);
		spinner.setVisibility(View.VISIBLE);
		List<String> list = new ArrayList<String>();

		// List available networks
		scan = wifi.getScanResults();
		for (ScanResult s : scan)
			list.add(s.SSID.toString());

		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, list);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		dataAdapter.notifyDataSetChanged();
		spinner.setAdapter(dataAdapter);

	}

	public void initDialog() {

		// custom dialog
		dialog = new Dialog(context);
		dialog.setContentView(R.layout.custom);
		dialog.setTitle("The Password is:");

		// set the custom dialog components - text, image and button
		edittext = (EditText) dialog.findViewById(R.id.edittext);
		edittext.setBackgroundColor(Color.BLACK);
		edittext.setText("Android custom dialog example!");
		image = (ImageView) dialog.findViewById(R.id.image);
		image.setImageResource(R.drawable.ic_launcher);

		Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
	}

	// This method is called at button click because we assigned the name to the
	// "On Click property" of the button
	public void myClickHandler(View view) {
		switch (view.getId()) {
		case R.id.buttonShowCustomDialog:

			if (text.getText().length() == 0) {
				Toast.makeText(getApplicationContext(),
						"Please Insert a Valid Mac-Address", Toast.LENGTH_LONG)
						.show();
				return;
			}

			initDialog();
			String key = new GenerateKey().generate(this.text.getText()
					.toString());

			if (key != null) {
				edittext.setText(key);
				dialog.show();
			} else
				Toast.makeText(context.getApplicationContext(),
						"Please Insert a Valid Mac-Address", Toast.LENGTH_LONG)
						.show();
			break;
		case R.id.buttonShowNetworks:

			if (wifi.getWifiState() == wifi.WIFI_STATE_ENABLED) {
				btn = (Button) findViewById(R.id.buttonShowCustomDialog);
				btn.setVisibility(View.GONE);
				addItemsOnSpinner();
				addListenerOnButton();
			}
			else
				Toast.makeText(getApplicationContext(),"Please Turn On Wi-Fi", Toast.LENGTH_LONG).show();		
			

			break;

		}

	}

}