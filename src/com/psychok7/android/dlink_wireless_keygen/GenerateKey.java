package com.psychok7.android.dlink_wireless_keygen;

public class GenerateKey {

	public GenerateKey() {

	}

	private char[] alpha_mACString_remove_separator(char[] mac, char[] str,
			String text) {
		int i, j;

		if (text.equalsIgnoreCase(""))
			mac = null;

		if (mac == null)
			return null;

		else {
			if (mac.length == 17) { // check if mac address size is correct
				for (j = 0, i = 0; i < 17; i++)
					if (mac[i] != ':' && mac[i] != '-' && mac[i] != '.'
							&& mac[i] != '_')
						str[j++] = mac[i];
			} else {
				return null;
			}
		}
		return str;
	}

	public String generate(String text) {

		char[] mac, key, newkey;
		mac = new char[12];
		key = new char[20];
		newkey = new char[20];
		int i, index;
		char t;
		char[] hash = { 'X', 'r', 'q', 'a', 'H', 'N', 'p', 'd', 'S', 'Y', 'w',
				'8', '6', '2', '1', '5' };

		mac = alpha_mACString_remove_separator(text.toCharArray(), mac, text);
		if (mac != null) {
			key[0] = mac[11];
			key[1] = mac[0];
			key[2] = mac[10];
			key[3] = mac[1];
			key[4] = mac[9];
			key[5] = mac[2];
			key[6] = mac[8];
			key[7] = mac[3];
			key[8] = mac[7];
			key[9] = mac[4];
			key[10] = mac[6];
			key[11] = mac[5];
			key[12] = mac[1];
			key[13] = mac[6];
			key[14] = mac[8];
			key[15] = mac[9];
			key[16] = mac[11];
			key[17] = mac[2];
			key[18] = mac[4];
			key[19] = mac[10];

			for (i = 0; i < 20; i++) {
				t = key[i];
				if ((t >= '0') && (t <= '9'))
					index = t - '0';
				else {
					t = Character.toUpperCase(t);
					if ((t >= 'A') && (t <= 'F'))
						index = t - 'A' + 10;
					else
						return null;
				}
				newkey[i] = hash[index];
			}
		} else
			return null;

		return new String(newkey);
	}

}
